# -*- coding: utf-8 -*-

from django.db import models
from django.conf import settings 
from django import forms
import yaml, sys
from django import conf

MODEL_CONFIG = settings.MODEL_CONFIG
MODEL_CHAR_MAX_LENGTH = settings.MODEL_CHAR_MAX_LENGTH

class DynamicModel(object):
    registry = {}
    
    def __init__(self, path_to_yaml= None):
        self.yaml = yaml.load(open((path_to_yaml or MODEL_CONFIG), 'r'))
        self.module = sys.modules[self.__module__]
        for name, params in self.yaml.items():
            self.create_model(name, params)
            
    def create_model(self, name, params):
        bases = (models.Model,)
        dct = dict()
        self.add_fields(params['fields'], dct)
        self.add_meta(params['title'], dct)
        dct['__unicode__'] = lambda self: u'%d - %s' % (self.id, params['title'].title())
        dct['__module__'] = self.__module__
        model = type(name.title(), bases, dct)
        self.registry[name] = model
        setattr(self.module, name.title(), model)
        self.create_form(model)
        
    def add_fields(self, fields, dct):
        dct['id'] = models.AutoField(primary_key= True)
        for field in fields:
            if field['type'] == 'int':
                clazz = models.IntegerField
                kw = dict()
            elif field['type'] == 'char':
                clazz = models.CharField
                kw = dict(max_length= MODEL_CHAR_MAX_LENGTH) 
            elif field['type'] == 'date':
                clazz = models.DateField     
                kw = dict(auto_now_add= False)
            else:
                continue
            obj = clazz(verbose_name= field['title'], **kw)
            obj._type = field['type']
            dct[field['id']] = obj

    def add_meta(self, title, dct):
        dct['Meta'] = type('Meta', tuple(), dict(
                verbose_name= title,
                verbose_name_plural= title,
                ordering = ('id',),
            )        
        )

    def create_form(self, model):
        model.Form = type('Form', (forms.ModelForm,), dict(
                Meta= type('Meta', tuple(), dict(model= model))
            )
        )        

dynamic_models = DynamicModel()    

            