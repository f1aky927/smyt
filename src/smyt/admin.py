# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import dynamic_models

models = dynamic_models.registry.values()
admin.site.register([model for model in models])