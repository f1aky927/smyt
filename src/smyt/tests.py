# -*- coding: utf-8 -*-

from django.utils import unittest
from django.test.client import Client
from .models import dynamic_models
from django.db.models import Model, CharField, IntegerField, DateField
from django.utils import simplejson as json
import datetime

class DynamicModelsCase(unittest.TestCase):

    def setUp(self):
        self.registry = dynamic_models.registry

    def test_name_models(self):
        self.assertEqual(self.registry.keys(), ['users', 'rooms'])

    def test_verbose_name(self):
        self.assertEqual(self.registry['rooms']._meta.verbose_name, u'Комнаты')
            
    def test_count_field(self):
        self.assertEqual(len(self.registry['users']._meta.fields), 4)

    def test_cls_field(self):
        self.assertTrue(isinstance(self.registry['users']._meta.fields[1], CharField))
        self.assertTrue(isinstance(self.registry['users']._meta.fields[3], DateField))
        self.assertTrue(isinstance(self.registry['rooms']._meta.fields[2], IntegerField))

    def test_name_field(self):    
        self.assertEqual(self.registry['rooms']._meta.fields[2].name, 'spots')

    def test_verbose_name_field(self):
        self.assertEqual(self.registry['users']._meta.fields[1].verbose_name, u'Имя')
    

AJAX = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}

class DynamicModelApiViewsCase(unittest.TestCase):

    def setUp(self):
        self.registry = dynamic_models.registry
        self.c = Client()
        self.user1 = self.registry['users'].objects.create(name= 'Jon', paycheck= 1000, date_joined= datetime.date.today())
        self.user2 = self.registry['users'].objects.create(name= 'Samanta', paycheck= 2000, date_joined= datetime.date.today())
        self.room1 = self.registry['rooms'].objects.create(department= 'Technical', spots= 100)
        
    def tearDown(self):
        self.user1.delete()
        self.user2.delete()
        self.room1.delete()


    def test_model_list(self):
        response = self.c.get('/model/list/', **AJAX) 
        data = json.loads(response._container[0])
        self.assertEqual(response['Content-Type'], 'application/json')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(data.has_key('data'))
        self.assertEqual(len(data['data']), 2)
        self.assertEqual(data['data'][0]['name'], 'users')
        self.assertEqual(data['data'][1]['title'], u'Комнаты')

    def test_model_table(self):
        response = self.c.get('/model/users/', **AJAX) 
        data = json.loads(response._container[0])
        self.assertEqual(response['Content-Type'], 'application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['name'], 'users')
        self.assertEqual(data['title'], u'Пользователи')
        self.assertEqual(len(data['fields']), 4)
        self.assertEqual(data['fields'][3]['name'], 'date_joined')
        self.assertEqual(data['fields'][1]['title'], u'Имя')
        self.assertEqual(data['fields'][2]['type'], 'int')
        self.assertEqual(len(data['values']), 2)
        self.assertEqual(len(data['values'][0]['fields']), 4)
        self.assertEqual(data['values'][0]['fields'][1]['value'], self.user1.name)
        self.assertEqual(data['values'][0]['fields'][2]['name'], 'paycheck')
        self.assertEqual(data['values'][0]['fields'][3]['id'], self.user1.id)
        
    def test_add_item(self):        
        response = self.c.get('/model/rooms/add/', dict(department= 'Manager', spots= 300), **AJAX) 
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.registry['rooms'].objects.count(), 2)
        
    def test_edit_item(self):
        response = self.c.get('/model/users/edit/', dict(id= self.user1.id, name= 'Bob'), **AJAX) 
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.registry['users'].objects.get(id= self.user1.id).name, 'Bob')
        
        