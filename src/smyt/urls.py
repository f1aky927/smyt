# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

urlpatterns = patterns('src.smyt.views',
    url(r'^$', 'index', name= 'index'),
    url(r'^model/list/$', 'model_list', name= 'model_list'),
    url(r'^model/(?P<name>\w+)/$', 'model_data', name= 'model_data'),
    url(r'^model/(?P<name>\w+)/add/$', 'model_add', name= 'model_add'),
    url(r'^model/(?P<name>\w+)/edit/$', 'model_edit', name= 'model_edit'),

)
