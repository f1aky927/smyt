#!/bin/bash
set -e
LOGFILE=/home/django/log/gunicorn
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=3
USER=django
GROUP=django
PORT=8001 
cd /home/django/smyt
source /home/django/smyt-env/bin/activate
test -d $LOGDIR || mkdir -p $LOGDIR
exec /home/django/smyt-env/bin/gunicorn_django -w $NUM_WORKERS \
  --user=$USER --group=$GROUP --log-level=debug \
  --log-file=$LOGFILE 2>>$LOGFILE \
  --bind 178.238.141.87:$PORT -D